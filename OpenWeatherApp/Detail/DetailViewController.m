//
//  DetailViewController.m
//  OpenWeatherApp
//
//  Created by Δημήτρης on 21/4/17.
//  Copyright © 2017 Dimitris Roilidis. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *cityNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *weatherNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *weatherDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *temperatureLabel;
@property (weak, nonatomic) IBOutlet UILabel *rainChanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *humidityLabel;
@property (weak, nonatomic) IBOutlet UILabel *pressureLabel;
@property (weak, nonatomic) IBOutlet UILabel *windSpeedLabel;
@property (weak, nonatomic) IBOutlet UILabel *windDirectionLabel;

@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *forecastFlowLayout;

@end

@implementation DetailViewController

- (void)viewDidLoad {
	
    [super viewDidLoad];
	
    // Do any additional setup after loading the view.
	if ([self.city.current.icon hasPrefix:@"01"] || [self.city.current.icon hasPrefix:@"02"] || [self.city.current.icon hasPrefix:@"10"])
		self.imageView.image = [UIImage imageNamed:self.city.current.icon];
	else
		self.imageView.image = [UIImage imageNamed:[self.city.current.icon substringToIndex:2]];
	
	self.cityNameLabel.text = self.city.cityName;
	self.weatherNameLabel.text = self.city.current.name;
	self.weatherDescriptionLabel.text = self.city.current.subname;
	self.temperatureLabel.text = [NSString stringWithFormat:@"%i˚", self.city.current.temperature.intValue];
	
	self.humidityLabel.text = [NSString stringWithFormat:@"%i%%", self.city.current.humidity.intValue];
	self.pressureLabel.text = [NSString stringWithFormat:@"%i hPa", self.city.current.pressure.intValue];
	self.windSpeedLabel.text = [NSString stringWithFormat:@"%i m/s", self.city.current.windSpeed.intValue];
	self.windDirectionLabel.text = [NSString stringWithFormat:@"%2.1f deg", self.city.current.windDirection.floatValue];
	
	// Rain chance requires some calculations looking into the future forecasts
	float rainChance = 0;
	NSMutableArray *nextTwelveHours = [NSMutableArray arrayWithObject:self.city.current];
	[nextTwelveHours addObjectsFromArray:[self.city.forecast filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"date < %@", [NSDate dateWithTimeIntervalSinceNow:12*3600]]]];
	for (WeatherData *data in nextTwelveHours)
		rainChance += data.hasRained;
	rainChance = 100*rainChance/nextTwelveHours.count;
	
	self.rainChanceLabel.text = [NSString stringWithFormat:@"%i%%", (int)rainChance];
	
	// Dynamically set the item size to be the screen smallest dimension, minus the inset
	CGFloat width = MIN([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
	self.forecastFlowLayout.itemSize = CGSizeMake(width-self.forecastFlowLayout.sectionInset.right-self.forecastFlowLayout.sectionInset.left, self.forecastFlowLayout.itemSize.height);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
	
	return self.city.condensedForecast.count;
}


#pragma mark - UICollectionViewDelegate

#define CELL_DATE_TAG	1
#define CELL_TEMP_TAG	2
#define CELL_NAME_TAG	3
#define CELL_DESC_TAG	4
#define CELL_ICON_TAG	5
#define CELL_HUMI_TAG	6
#define CELL_PRES_TAG	7
#define CELL_WIND_TAG	8
#define CELL_DIRE_TAG	9

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
	
	UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"forecast.cell" forIndexPath:indexPath];
	
	cell.layer.cornerRadius = 5;
	cell.layer.masksToBounds = YES;
	
	WeatherData *weather = [self.city.condensedForecast objectAtIndex:indexPath.row];
	
	// Date label: tag 1
	UILabel *dateLabel = (UILabel *)[cell viewWithTag:CELL_DATE_TAG];
	// Date in the form MM<linebreak>dd
	NSDateFormatter *formatter = [NSDateFormatter new];
	formatter.dateFormat = [NSDateFormatter dateFormatFromTemplate:@"MMM d" options:0 locale:[NSLocale currentLocale]];
	dateLabel.text = [[[formatter stringFromDate:weather.date] uppercaseString] stringByReplacingOccurrencesOfString:@" " withString:@"\n"];
	
	// Temperature label: tag 2
	UILabel *temperatureLabel = (UILabel *)[cell viewWithTag:CELL_TEMP_TAG];
	temperatureLabel.text = [NSString stringWithFormat:@"%i˚", weather.temperature.intValue];
	
	// Weather name label: tag 3
	UILabel *weatherNameLabel = (UILabel *)[cell viewWithTag:CELL_NAME_TAG];
	weatherNameLabel.text = weather.name;
	
	// Weather description label: tag 4
	UILabel *weatherDescriptionLabel = (UILabel *)[cell viewWithTag:CELL_DESC_TAG];
	weatherDescriptionLabel.text = weather.subname;
	
	// Image view: tag 5
	UIImageView *imageView = (UIImageView *)[cell viewWithTag:CELL_ICON_TAG];
	if ([weather.icon hasPrefix:@"01"] || [weather.icon hasPrefix:@"02"] || [weather.icon hasPrefix:@"10"])
		imageView.image = [UIImage imageNamed:weather.icon];
	else
		imageView.image = [UIImage imageNamed:[weather.icon substringToIndex:2]];
	
	// Humidity label: tag 6
	UILabel *humidityLabel = (UILabel *)[cell viewWithTag:CELL_HUMI_TAG];
	humidityLabel.text = [NSString stringWithFormat:@"%i%%", weather.humidity.intValue];
	
	// Humidity label: tag 7
	UILabel *pressureLabel = (UILabel *)[cell viewWithTag:CELL_PRES_TAG];
	pressureLabel.text = [NSString stringWithFormat:@"%i hPa", weather.pressure.intValue];
	
	// Humidity label: tag 8
	UILabel *windSpeedLabel = (UILabel *)[cell viewWithTag:CELL_WIND_TAG];
	windSpeedLabel.text = [NSString stringWithFormat:@"%i m/s", weather.windSpeed.intValue];
	
	// Humidity label: tag 9
	UILabel *windDirectionLabel = (UILabel *)[cell viewWithTag:CELL_DIRE_TAG];
	windDirectionLabel.text = [NSString stringWithFormat:@"%i deg", weather.windDirection.intValue];
	
	
	return cell;
}


@end
