//
//  SearchResultsViewController.h
//  OpenWeatherApp
//
//  Created by Δημήτρης on 23/4/17.
//  Copyright © 2017 Dimitris Roilidis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultsViewController : UIViewController

@property (nonatomic) NSString *searchTerm;
@property (weak, nonatomic) NSArray *cities;

@end
