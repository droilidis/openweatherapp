//
//  SearchResultsViewController.m
//  OpenWeatherApp
//
//  Created by Δημήτρης on 23/4/17.
//  Copyright © 2017 Dimitris Roilidis. All rights reserved.
//

#import "SearchResultsViewController.h"

@interface SearchResultsViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) NSArray *filteredCities;

@end

@implementation SearchResultsViewController

- (void)viewDidLoad {
	
    [super viewDidLoad];
	
    // Do any additional setup after loading the view from its nib.
	UINib *cellNib = [UINib nibWithNibName:@"SearchCityCell" bundle:[NSBundle mainBundle]];
	[self.tableView registerNib:cellNib forCellReuseIdentifier:@"search.city.cell"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)setSearchTerm:(NSString *)searchTerm {
	
	// Update filtered only if new search term
	if ([_searchTerm isEqualToString:searchTerm] == NO && self.cities) {
		
		self.filteredCities = [self.cities filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", searchTerm]];
	}
	
	_searchTerm = searchTerm;
}

- (void)setFilteredCities:(NSArray *)filteredCities {
	
	_filteredCities = filteredCities;
	
	[self.tableView reloadData];
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	return self.filteredCities.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"search.city.cell" forIndexPath:indexPath];
	
	NSDictionary *cityJSON = self.filteredCities[indexPath.row];
	
	// Configure the cell...
	cell.textLabel.text = cityJSON[@"name"];
	cell.accessoryType = [[cityJSON valueForKey:@"selected"] boolValue] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
	
	return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	// Selecting a cell toggles the "selected" status
	NSMutableDictionary *cityJSON = self.filteredCities[indexPath.row];
	
	[cityJSON setValue:@(![cityJSON[@"selected"] boolValue]) forKey:@"selected"];
	
	[tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

@end
