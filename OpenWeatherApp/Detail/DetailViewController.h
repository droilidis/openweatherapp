//
//  DetailViewController.h
//  OpenWeatherApp
//
//  Created by Δημήτρης on 21/4/17.
//  Copyright © 2017 Dimitris Roilidis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CityData.h"

@interface DetailViewController : UIViewController

@property (strong, nonatomic) CityData *city;

@end
