//
//  SearchViewController.m
//  OpenWeatherApp
//
//  Created by Δημήτρης on 23/4/17.
//  Copyright © 2017 Dimitris Roilidis. All rights reserved.
//

#import "SearchViewController.h"
#import "SearchResultsViewController.h"
#import "CityManager.h"

@interface SearchViewController () <UISearchResultsUpdating, UISearchControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UISearchController *searchController;

@property (readonly, nonatomic) NSArray *cities;

@end

@implementation SearchViewController

- (void)viewDidLoad {
	
    [super viewDidLoad];
	
	// Load the cities data
	NSError *error = nil;
	NSURL *citiesURL = [[NSBundle mainBundle] URLForResource:@"city.list" withExtension:@"json" subdirectory:@"JSON"];
	NSData *jsonData = [NSData dataWithContentsOfURL:citiesURL options:0 error:&error];
	
	NSJSONSerialization *json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
	if (json && [json isKindOfClass:NSArray.class])
		_cities = [(NSArray *)json sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]]];
	else
		_cities = @[];
	
    // Do any additional setup after loading the view.
	// Create the search controller and store a reference to it.
	SearchResultsViewController *resultsController = [SearchResultsViewController new];
	resultsController.cities = self.cities;
	self.searchController = [[UISearchController alloc] initWithSearchResultsController:resultsController];
 
	// Use the current view controller to update the search results.
	self.searchController.searchResultsUpdater = self;
	self.searchController.delegate = self;
	self.searchController.hidesNavigationBarDuringPresentation = NO;
	
	// Install the search bar as the table header.
	self.tableView.tableHeaderView = self.searchController.searchBar;
 
	// It is usually good to set the presentation context.
	self.definesPresentationContext = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - Actions

- (IBAction)save:(id)sender {
	
	// Save the selected cities
	NSArray *selectedCities = [self.cities filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"selected == TRUE"]];
	for (NSDictionary *cityJSON in selectedCities) {
		
		NSString *cityID = [cityJSON[@"id"] stringValue];
		NSString *cityName = cityJSON[@"name"];
		CLLocationDegrees latitude = [[cityJSON valueForKeyPath:@"coord.lat"] floatValue];
		CLLocationDegrees longitude = [[cityJSON valueForKeyPath:@"coord.lon"] floatValue];
		
		[[CityManager sharedManager] addCityWithID:cityID andName:cityName andCoordinates:CLLocationCoordinate2DMake(latitude, longitude)];
	}
	
	[self dismissViewControllerAnimated:YES completion:^{
		
	}];
}

- (IBAction)cancel:(id)sender {
	
	[self dismissViewControllerAnimated:YES completion:^{
		
	}];
}


#pragma mark - UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
	
	NSString *searchTerm = searchController.searchBar.text;
	if ([searchTerm isEqualToString:@""] == NO)
		[searchController.searchResultsController setValue:searchTerm forKey:@"searchTerm"];
	
//	NSArray *filteredCities = [self.cities filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", searchTerm]];
//	[searchController.searchResultsController setValue:filteredCities forKey:@"filteredCities"];
}


#pragma mark - UISearchControllerDelegate

- (void)didDismissSearchController:(UISearchController *)searchController {
	
	[self.tableView reloadData];
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	return self.cities.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"search.city.cell" forIndexPath:indexPath];
	
	NSDictionary *cityJSON = self.cities[indexPath.row];
	
	// Configure the cell...
	cell.textLabel.text = cityJSON[@"name"];
	cell.accessoryType = [[cityJSON valueForKey:@"selected"] boolValue] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
	
	return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	// Selecting a cell toggles the "selected" status
	NSMutableDictionary *cityJSON = self.cities[indexPath.row];
	
	[cityJSON setValue:@(![cityJSON[@"selected"] boolValue]) forKey:@"selected"];
	
	[tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}


@end
