//
//  HelpViewController.m
//  OpenWeatherApp
//
//  Created by Δημήτρης on 22/4/17.
//  Copyright © 2017 Dimitris Roilidis. All rights reserved.
//

#import "HelpViewController.h"

@import WebKit;

@interface HelpViewController ()

@property (weak, nonatomic) IBOutlet UIView *webContainerView;
@property (strong) WKWebView *webview;

@end

@implementation HelpViewController

- (void)viewDidLoad {
	
    [super viewDidLoad];
	
    // Do any additional setup after loading the view.
	// Add the web view
	self.webview = [WKWebView new];
	self.webview.translatesAutoresizingMaskIntoConstraints = NO;
	
	[self.webContainerView addSubview:self.webview];
	
	// Install constraints to pin to container
	[self.webContainerView addConstraint:[NSLayoutConstraint constraintWithItem:self.webview
																	  attribute:NSLayoutAttributeTop
																	  relatedBy:NSLayoutRelationEqual
																		 toItem:self.webContainerView
																	  attribute:NSLayoutAttributeTopMargin
																	 multiplier:1
																	   constant:0.0]];
	[self.webContainerView addConstraint:[NSLayoutConstraint constraintWithItem:self.webview
																	  attribute:NSLayoutAttributeBottom
																	  relatedBy:NSLayoutRelationEqual
																		 toItem:self.webContainerView
																	  attribute:NSLayoutAttributeBottomMargin
																	 multiplier:1
																	   constant:0.0]];
	[self.webContainerView addConstraint:[NSLayoutConstraint constraintWithItem:self.webview
																	  attribute:NSLayoutAttributeLeading
																	  relatedBy:NSLayoutRelationEqual
																		 toItem:self.webContainerView
																	  attribute:NSLayoutAttributeLeadingMargin
																	 multiplier:1
																	   constant:0.0]];
	[self.webContainerView addConstraint:[NSLayoutConstraint constraintWithItem:self.webview
																	  attribute:NSLayoutAttributeTrailing
																	  relatedBy:NSLayoutRelationEqual
																		 toItem:self.webContainerView
																	  attribute:NSLayoutAttributeTrailingMargin
																	 multiplier:1
																	   constant:0.0]];
	
	NSURL *helpURL = [[NSBundle mainBundle] URLForResource:@"index" withExtension:@"html" subdirectory:@"HTML"];
	[self.webview loadFileURL:helpURL allowingReadAccessToURL:[helpURL URLByDeletingLastPathComponent]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)dismiss {

	[self dismissViewControllerAnimated:YES completion:^{
		
	}];
}

@end
