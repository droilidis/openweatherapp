//
//  CitiesTableViewCell.h
//  OpenWeatherApp
//
//  Created by Δημήτρης on 21/4/17.
//  Copyright © 2017 Dimitris Roilidis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CityData.h"

@interface CitiesTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *temperatureLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityNameLabel;

@property (strong, nonatomic) CityData *city;

@end
