//
//  WeatherData.h
//  OpenWeatherApp
//
//  Created by Δημήτρης on 21/4/17.
//  Copyright © 2017 Dimitris Roilidis. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WeatherData : NSObject <NSCoding>

@property (readonly) NSDate *date;

//@property (readonly) NSString *cityName;
//@property (readonly) NSString *cityID;

@property (readonly) NSString *code;			// weather code ID
@property (readonly) NSString *name;			// weather group name, e.g Thunderstrom
@property (readonly) NSString *subname;			// weather meaning/description, e.g thunderstorm with light rain
@property (readonly) NSString *icon;

@property (readonly) NSString *temperature;
@property (readonly) NSString *humidity;
@property (readonly) NSString *pressure;

@property (readonly) NSString *windSpeed;
@property (readonly) NSString *windDirection;

@property (readonly) float precipation;			// mm of rain in past 3 hours
@property (readonly) BOOL hasRained;			// has rained in past 3 hours?

- (instancetype)initWithDictionary:(NSDictionary *)json;

@end
