//
//  WeatherData.m
//  OpenWeatherApp
//
//  Created by Δημήτρης on 21/4/17.
//  Copyright © 2017 Dimitris Roilidis. All rights reserved.
//

#import "WeatherData.h"

@implementation WeatherData

- (instancetype)initWithDictionary:(NSDictionary *)json {
	
	if (self = [super init]) {
		
		_date = [NSDate dateWithTimeIntervalSince1970:[[json valueForKeyPath:@"dt"] doubleValue]];
		
//		_cityName = [json valueForKeyPath:@"name"];
//		_cityID = [[json valueForKeyPath:@"id"] stringValue];
		
		_code = [[[json valueForKeyPath:@"weather.id"] firstObject] stringValue];
		_name = [[json valueForKeyPath:@"weather.main"] firstObject];
		_subname = [[json valueForKeyPath:@"weather.description"] firstObject];
		_icon = [[json valueForKeyPath:@"weather.icon"] firstObject];
		
		_temperature = [[json valueForKeyPath:@"main.temp"] stringValue];
		_humidity = [[json valueForKeyPath:@"main.humidity"] stringValue];
		_pressure = [[json valueForKeyPath:@"main.pressure"] stringValue];
		
		_windSpeed = [[json valueForKeyPath:@"wind.speed"] stringValue];
		_windDirection = [[json valueForKeyPath:@"wind.deg"] stringValue];
		
		_precipation = [[json valueForKeyPath:@"rain.3h"] floatValue];
	}
	
	return self;
}


#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)decoder {
	
	if (self != nil) {

		_date = [decoder decodeObjectForKey:@"date"];
		
//		_cityName = [decoder decodeObjectForKey:@"cityName"];
//		_cityID = [decoder decodeObjectForKey:@"cityID"];
		
		_code = [decoder decodeObjectForKey:@"code"];
		_name = [decoder decodeObjectForKey:@"name"];
		_subname = [decoder decodeObjectForKey:@"subname"];
		_icon = [decoder decodeObjectForKey:@"icon"];
		
		_temperature = [decoder decodeObjectForKey:@"temperature"];
		_humidity = [decoder decodeObjectForKey:@"humidity"];
		_pressure = [decoder decodeObjectForKey:@"pressure"];
		
		_windSpeed = [decoder decodeObjectForKey:@"windSpeed"];
		_windDirection = [decoder decodeObjectForKey:@"windDirection"];
		
		_precipation = [[decoder decodeObjectForKey:@"precipation"] floatValue];
	}
	
	return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
	
	[encoder encodeObject:_date forKey:@"date"];
	
//	[encoder encodeObject:_cityName forKey:@"cityName"];
//	[encoder encodeObject:_cityID forKey:@"cityID"];
	
	[encoder encodeObject:_code forKey:@"code"];
	[encoder encodeObject:_name forKey:@"name"];
	[encoder encodeObject:_subname forKey:@"subname"];
	[encoder encodeObject:_icon forKey:@"icon"];
	
	[encoder encodeObject:_temperature forKey:@"temperature"];
	[encoder encodeObject:_humidity forKey:@"humidity"];
	[encoder encodeObject:_pressure forKey:@"pressure"];
	
	[encoder encodeObject:_windSpeed forKey:@"windSpeed"];
	[encoder encodeObject:_windDirection forKey:@"windDirection"];
	
	[encoder encodeObject:@(_precipation) forKey:@"precipation"];
}

@end
