//
//  CityManager.h
//  OpenWeatherApp
//
//  Created by Δημήτρης on 21/4/17.
//  Copyright © 2017 Dimitris Roilidis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CityData.h"

@interface CityManager : NSObject

@property (readonly) NSArray<CityData *> *cities;

+ (instancetype)sharedManager;

- (void)addCityWithID:(NSString *)cityID andName:(NSString *)cityName andCoordinates:(CLLocationCoordinate2D)coords;
- (void)removeCity:(CityData *)city;

- (BOOL)save:(NSError *)error;

@end
