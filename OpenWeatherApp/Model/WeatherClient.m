//
//  WeatherClient.m
//  OpenWeatherApp
//
//  Created by Δημήτρης on 21/4/17.
//  Copyright © 2017 Dimitris Roilidis. All rights reserved.
//

#import "WeatherClient.h"

@interface WeatherClient () <NSURLSessionDelegate> {
	
	NSString *_baseURL;
	NSString *_apiKey;
	NSString *_apiVersion;
	NSString *_language;
	NSString *_units;
}

@end

@implementation WeatherClient

+ (instancetype)sharedClient {
	
	static WeatherClient *client = nil;
	static dispatch_once_t onceToken;
	
	dispatch_once(&onceToken, ^{
		client = [[WeatherClient alloc] init];
		[client singletonInit];
	});
	
	return client;
}

- (void)singletonInit {
	
	_baseURL = @"http://api.openweathermap.org/data/";
	_apiKey  = @"c6e381d8c7ff98f0fee43775817cf6ad";
	_apiVersion = @"2.5";
	_language = @"en";
	_units = @"metric";
}

- (void)searchNearestCityForCoordinates:(CLLocationCoordinate2D)coords
						   withCallback:( void (^)( NSError* error, NSString *cityID, NSString *cityName ) )callback {
	
	NSOperationQueue *callingQueue = [NSOperationQueue currentQueue];
	NSDictionary *parameters = @{@"lat" : @(coords.latitude),
								 @"lon" : @(coords.longitude),
								 @"cnt" : @1, };
	
	[self callMethod:@"find" withParameters:parameters andCallback:^(NSError *error, NSDictionary *result) {
		
		if (error) {
			
			[callingQueue addOperationWithBlock:^{
				callback(error, nil, nil);
			}];
			return;
		}
		
		NSString *cityID = [NSString stringWithFormat:@"%@", [[result valueForKeyPath:@"list.id"] firstObject]];
		NSString *cityName = [NSString stringWithFormat:@"%@", [[result valueForKeyPath:@"list.name"] firstObject]];
		
		[callingQueue addOperationWithBlock:^{
			callback(nil, cityID, cityName);
		}];
	}];
}

- (void)weatherForCoordinates:(CLLocationCoordinate2D)coords
				 withCallback:( void (^)( NSError* error, WeatherData *data ) )callback {
	
	NSOperationQueue *callingQueue = [NSOperationQueue currentQueue];
	NSDictionary *parameters = @{@"lat" : @(coords.latitude),
								 @"lon" : @(coords.longitude),
								 @"cnt" : @1, };
	
	[self callMethod:@"weather" withParameters:parameters andCallback:^(NSError *error, NSDictionary *result) {
		
		if (error) {
			
			[callingQueue addOperationWithBlock:^{
				callback(error, nil);
			}];
			return;
		}
		
		WeatherData *data = [[WeatherData new] initWithDictionary:result];
		[callingQueue addOperationWithBlock:^{
			callback(nil, data);
		}];
	}];
}

- (void)weatherForCityID:(NSString *)cityID
			withCallback:( void (^)( NSError* error, WeatherData *data ) )callback {
	
	NSOperationQueue *callingQueue = [NSOperationQueue currentQueue];
	NSDictionary *parameters = @{@"id" : cityID};
	
	[self callMethod:@"weather" withParameters:parameters andCallback:^(NSError *error, NSDictionary *result) {
		
		if (error) {
			
			[callingQueue addOperationWithBlock:^{
				callback(error, nil);
			}];
			return;
		}
		
		WeatherData *data = [[WeatherData new] initWithDictionary:result];
		[callingQueue addOperationWithBlock:^{
			callback(nil, data);
		}];
	}];
}

- (void)forecastForCityID:(NSString *)cityID
			 withCallback:(void (^)( NSError* error, NSArray<WeatherData *> *dataArray )) callback {
	
	NSOperationQueue *callingQueue = [NSOperationQueue currentQueue];
	NSDictionary *parameters = @{@"id" : cityID};
	
	[self callMethod:@"forecast" withParameters:parameters andCallback:^(NSError *error, NSDictionary *result) {
		
		if (error) {
			
			[callingQueue addOperationWithBlock:^{
				callback(error, nil);
			}];
			return;
		}
		
		NSMutableArray *returnArray = [NSMutableArray array];
		
		NSArray *dataPoints = [result objectForKeyedSubscript:@"list"];
		for (NSDictionary *dataPoint in dataPoints) {
			
			WeatherData *data = [[WeatherData new] initWithDictionary:dataPoint];
			[returnArray addObject:data];
			
		}
		
		[callingQueue addOperationWithBlock:^{
			callback(nil, returnArray);
		}];
	}];
}


#pragma mark - Private

- (void)callMethod:(NSString *)method withParameters:(NSDictionary *)parameters
	   andCallback:( void (^)( NSError* error, NSDictionary *result ) )callback {
	
	// Construct URL with default components
	NSURLComponents *components = [NSURLComponents componentsWithString:_baseURL];
	components.path = [components.path stringByAppendingPathComponent:_apiVersion];
	components.path = [components.path stringByAppendingPathComponent:method];
	
	NSMutableArray *queryItems = [NSMutableArray arrayWithObjects:
								  [NSURLQueryItem queryItemWithName:@"appid" value:_apiKey],
								  [NSURLQueryItem queryItemWithName:@"lang" value:_language],
								  [NSURLQueryItem queryItemWithName:@"units" value:_units], nil];
	
	// And add custom parameters for this call
	[parameters enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
		
		[queryItems addObject:[NSURLQueryItem queryItemWithName:key value:[NSString stringWithFormat:@"%@", obj]]];
	}];
	
	components.queryItems = queryItems;
	
	NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
	NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
	
	
	NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:components.URL
													completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
														
														if (error == nil) {
															
															NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
															callback(nil, json);
															
														} else {
															callback(error, nil);
														}
														
													}];
	
	[dataTask resume];
}


@end
