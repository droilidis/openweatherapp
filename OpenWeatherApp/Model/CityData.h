//
//  CityData.h
//  OpenWeatherApp
//
//  Created by Δημήτρης on 21/4/17.
//  Copyright © 2017 Dimitris Roilidis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WeatherData.h"

@import CoreLocation;

@interface CityData : NSObject <NSCoding>

@property (readonly) NSDate *lastUpdated;

@property (readonly) NSString *cityID;
@property (readonly) NSString *cityName;
@property (readonly) double latitude;
@property (readonly) double longitude;
@property (readonly) CLLocationCoordinate2D coordinates;

@property (readonly, strong) WeatherData *current;
@property (readonly, strong) NSArray<WeatherData *> *forecast;

- (instancetype)initWithCoordinates:(CLLocationCoordinate2D)coords andName:(NSString *)cityName andID:(NSString *)cityID;

- (void)updateWithCallback:(void (^)( NSError *error )) callback;

- (NSArray<WeatherData *> *)condensedForecast;

@end
