//
//  HomeViewController.m
//  OpenWeatherApp
//
//  Created by Δημήτρης on 21/4/17.
//  Copyright © 2017 Dimitris Roilidis. All rights reserved.
//

#import "HomeViewController.h"
#import "CitiesTableViewController.h"
#import "WeatherClient.h"
#import "CityManager.h"

@import MapKit;

@interface HomeViewController () <MKMapViewDelegate, CLLocationManagerDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property MKPointAnnotation *pin;
@property NSString *pinName;
@property NSString *pinID;

@property (strong) CLLocationManager *locationManager;

@end

@implementation HomeViewController

- (void)viewDidLoad {
	
    [super viewDidLoad];
	
    // Do any additional setup after loading the view.
	self.mapView.delegate = self;
	
	// Ask for location permission
	CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
	if (status != kCLAuthorizationStatusRestricted && status != kCLAuthorizationStatusDenied) {
		
		self.locationManager = [CLLocationManager new];
		self.locationManager.delegate = self;
		
		if (status == kCLAuthorizationStatusNotDetermined)
			[self.locationManager requestWhenInUseAuthorization];
		else
			self.mapView.showsUserLocation = YES;
	}
	
	// Add long press gesture recognizer to drop pin
	UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc]
											   initWithTarget:self action:@selector(handleLongPress:)];
	longPress.minimumPressDuration = 1.0;
	[self.mapView addGestureRecognizer:longPress];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
	if ([segue.identifier isEqualToString:@"home.embed.cities"]) {
		
		// When the home controller is embedding the cities table controller,
		// take this chance to loosely couple them
		CitiesTableViewController *citiesTVC = (CitiesTableViewController *)segue.destinationViewController;
		citiesTVC.selectionCallback = ^void (CityData *city) {
		
			// Update map
			[self.mapView setCenterCoordinate:city.coordinates animated:YES];
		};
	}
}


#pragma mark - Actions

- (void)addCityFromCurrentPin {
	
	// Add the city
	// Updates will be triggered via KVO
	[[CityManager sharedManager] addCityWithID:self.pinID andName:self.pinName andCoordinates:self.pin.coordinate];
	
	// Remove the pin from the map
	[self.mapView removeAnnotation:self.pin];
}


#pragma mark - Gestures

- (void)handleLongPress:(UIGestureRecognizer *)gestureRecognizer {
	
	if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
		return;
	
	CGPoint touchPoint = [gestureRecognizer locationInView:self.mapView];
	CLLocationCoordinate2D touchMapCoordinate =
	[self.mapView convertPoint:touchPoint toCoordinateFromView:self.mapView];
	
	MKPointAnnotation *newPin = [[MKPointAnnotation alloc] init];
	newPin.coordinate = touchMapCoordinate;
	
	// Temporarily name the pin while we fetch the actual city
	newPin.title = @"Fetching city";
	[[WeatherClient sharedClient] searchNearestCityForCoordinates:touchMapCoordinate withCallback:^(NSError *error, NSString *cityID, NSString *cityName) {
		
		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
			
			MKAnnotationView *pinView = [self.mapView viewForAnnotation:self.pin];
			
			if (error) {
				
				self.pin.title = @"Error fetching city";
				pinView.rightCalloutAccessoryView = nil;
			}
			
			else {
				
				self.pinName = cityName;
				self.pinID = cityID;
				
				self.pin.title = cityName;
				
				UIButton *addButton = [UIButton buttonWithType:UIButtonTypeContactAdd];
				[addButton addTarget:self action:@selector(addCityFromCurrentPin) forControlEvents:UIControlEventTouchUpInside];
				pinView.rightCalloutAccessoryView = addButton;
			}
		});
	}];
	
	// New pin replaces previous one
	[self.mapView removeAnnotation:self.pin];
	
	self.pin = newPin;
	[self.mapView addAnnotation:self.pin];
	
	// Select the new pin
	[self.mapView selectAnnotation:self.pin animated:YES];
}


#pragma mark - MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
	
	[self.mapView setCenterCoordinate:userLocation.location.coordinate animated:YES];
	
	// Stop tracking location
	self.mapView.showsUserLocation = NO;
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
	
	MKPinAnnotationView *pinView = (MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"dropped.pin"];
	if (pinView == nil) {
		
		pinView = [[MKPinAnnotationView alloc] initWithAnnotation:self.pin reuseIdentifier:@"dropped.pin"];
		pinView.pinTintColor = [MKPinAnnotationView redPinColor];
		pinView.animatesDrop = YES;
		pinView.canShowCallout = YES;
	}
	
	// Pin always starts with a spinner. It will be changed later when the weather call ends
	UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	[spinner startAnimating];
	pinView.rightCalloutAccessoryView = spinner;
	
	return pinView;
}


#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
	
	if (status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse)
		self.mapView.showsUserLocation = YES;
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
	
	NSLog(@"Location error: %@", error);
}


@end




