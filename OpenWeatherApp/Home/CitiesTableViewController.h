//
//  CitiesTableViewController.h
//  OpenWeatherApp
//
//  Created by Δημήτρης on 21/4/17.
//  Copyright © 2017 Dimitris Roilidis. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CityData;

@interface CitiesTableViewController : UITableViewController

@property (nonatomic, copy) void (^selectionCallback)(CityData *city);

@end
