//
//  CityData.m
//  OpenWeatherApp
//
//  Created by Δημήτρης on 21/4/17.
//  Copyright © 2017 Dimitris Roilidis. All rights reserved.
//

#import "CityData.h"
#import "WeatherClient.h"

@implementation CityData

- (instancetype)initWithCoordinates:(CLLocationCoordinate2D)coords andName:(NSString *)cityName andID:(NSString *)cityID {
	
	if (self = [super init]) {
		
		_coordinates = coords;
		_cityID = [NSString stringWithString:cityID];
		_cityName = [NSString stringWithString:cityName];
		
		[self commonInit];
	}
	
	return self;
}

- (void)commonInit {
	
	// Set up a timer to periodically refresh
	// Around every 10 minutes (that's how fresh the data is)
	int sign = arc4random_uniform(2);
	NSTimeInterval interval = 10*60 + (arc4random_uniform(30) * (sign ? 1.0 : -1.0));
	[NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(update:) userInfo:NULL repeats:YES];
	
	// And also trigger one immediately
	[self updateWithCallback:nil];
}

- (void)update:(NSTimer *)timer {
	
	[self updateWithCallback:nil];
}

- (void)updateWithCallback:(void (^)( NSError *error )) callback {
	
	NSLog(@"Updating city: %@", _cityName);

	NSOperationQueue *callingQueue = [NSOperationQueue currentQueue];
	
	// Current data is updated every 5 minutes
	if (_current == nil || _lastUpdated == nil || [_lastUpdated timeIntervalSinceNow] < -5*60) {
		
		// Fetch current weather
		[[WeatherClient sharedClient] weatherForCityID:_cityID withCallback:^(NSError *error, WeatherData *data) {
			
			[self willChangeValueForKey:@"current"];
			_current = data;
			[self didChangeValueForKey:@"current"];
			_lastUpdated = [NSDate date];
			
			// After fetching the current weather, maybe we want the forecast as well
			// Forecast is old if the earliest forecast is one hour in the past
			if (_forecast == nil || _forecast.count == 0 || [_forecast.firstObject.date timeIntervalSinceNow] < -60*60) {
				
				// Fetch forecast
				[[WeatherClient sharedClient] forecastForCityID:_cityID withCallback:^(NSError *error, NSArray<WeatherData *> *dataArray) {
					
					[self willChangeValueForKey:@"forecast"];
					_forecast = dataArray;
					[self didChangeValueForKey:@"forecast"];
					
					if (callback)
						[callingQueue addOperationWithBlock:^{
							callback(error);
						}];
				}];
			}
			
			// No forecast update needed, call back
			else {
				
				if (callback)
					[callingQueue addOperationWithBlock:^{
						callback(error);
					}];
			}
		}];
	}
}


- (NSArray<WeatherData *> *)condensedForecast {
	
	NSMutableArray *returnArray = [NSMutableArray array];
	NSCalendar *calendar = [NSCalendar currentCalendar];
	
	// For every day in the forecast, keep only one datapoint at midday
	[_forecast enumerateObjectsUsingBlock:^(WeatherData * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
		
		NSDateComponents *components = [calendar components:(NSCalendarUnitHour) fromDate:obj.date];
		NSInteger hour = [components hour];
		if (hour == 12)
			[returnArray addObject:obj];
	}];
	
	return returnArray;
}


#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)decoder {
	
	if (self != nil) {
		
		_lastUpdated = [decoder decodeObjectForKey:@"lastUpdated"];
		
		_cityName = [decoder decodeObjectForKey:@"cityName"];
		_cityID = [decoder decodeObjectForKey:@"cityID"];
		
		CLLocationDegrees latitude = [[decoder decodeObjectForKey:@"latitude"] doubleValue];
		CLLocationDegrees longitude = [[decoder decodeObjectForKey:@"longitude"] doubleValue];
		_coordinates = CLLocationCoordinate2DMake(latitude, longitude);
		
		NSData *currentData = [decoder decodeObjectForKey:@"current"];
		if (!currentData)
			NSLog(@"Failed to decode current weather");
		else
			_current = [NSKeyedUnarchiver unarchiveObjectWithData:currentData];
		
		NSData *forecastData = [decoder decodeObjectForKey:@"forecast"];
		if (!forecastData)
			NSLog(@"Failed to decode forecast weather");
		else
			_forecast = [NSKeyedUnarchiver unarchiveObjectWithData:forecastData];
		
		[self commonInit];
	}
	
	return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
	
	[encoder encodeObject:_lastUpdated forKey:@"lastUpdated"];
	
	[encoder encodeObject:_cityName forKey:@"cityName"];
	[encoder encodeObject:_cityID forKey:@"cityID"];
	[encoder encodeObject:@(_coordinates.latitude) forKey:@"latitude"];
	[encoder encodeObject:@(_coordinates.longitude) forKey:@"longitude"];
	
	NSData *currentData = [NSKeyedArchiver archivedDataWithRootObject:_current];
	if (!currentData)
		NSLog(@"Failed to archive current weather: %@", _current);
	else
		[encoder encodeObject:currentData forKey:@"current"];
	
	NSData *forecastData = [NSKeyedArchiver archivedDataWithRootObject:_forecast];
	if (!forecastData)
		NSLog(@"Failed to archive forecast weather: %@", _forecast);
	else
		[encoder encodeObject:forecastData forKey:@"forecast"];
	
}

@end




