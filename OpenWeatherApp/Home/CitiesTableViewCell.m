//
//  CitiesTableViewCell.m
//  OpenWeatherApp
//
//  Created by Δημήτρης on 21/4/17.
//  Copyright © 2017 Dimitris Roilidis. All rights reserved.
//

#import "CitiesTableViewCell.h"

@implementation CitiesTableViewCell

- (void)awakeFromNib {
	
	[super awakeFromNib];
	
	// Initialization code
}

- (void)dealloc {
	
	@try {
		[_city removeObserver:self forKeyPath:@"current"];
	} @catch (NSException *exception) {}
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCity:(CityData *)city {
	
	if (city == _city)
		return;
	
	// Stop observing previous city
	@try {
		[_city removeObserver:self forKeyPath:@"current"];
	} @catch (NSException *exception) {}
	
	_city = city;
	
	// Start observing new city
	[_city addObserver:self forKeyPath:@"current" options:0 context:nil];
	
	// Set the labels
	self.cityNameLabel.text = _city.cityName;
	self.temperatureLabel.text = [NSString stringWithFormat:@"%i˚", _city.current.temperature.intValue];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
	
	if (object == _city && [keyPath isEqualToString:@"current"]) {
		
		self.cityNameLabel.text = _city.cityName;
		self.temperatureLabel.text = [NSString stringWithFormat:@"%i˚", _city.current.temperature.intValue];
	}
}

- (void)prepareForReuse {
	
	[super prepareForReuse];
	
	// Stop observing city
	@try {
		[_city removeObserver:self forKeyPath:@"current"];
	} @catch (NSException *exception) {}
}

@end
