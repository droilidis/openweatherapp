//
//  CityManager.m
//  OpenWeatherApp
//
//  Created by Δημήτρης on 21/4/17.
//  Copyright © 2017 Dimitris Roilidis. All rights reserved.
//

#import "CityManager.h"

@interface CityManager () {
	
	NSMutableArray *_mutableCities;
}

@property (readonly) NSString *filepath;

@end

@implementation CityManager

+ (instancetype)sharedManager {
	
	static CityManager *manager = nil;
	static dispatch_once_t onceToken;
	
	dispatch_once(&onceToken, ^{
		manager = [[CityManager alloc] init];
		[manager singletonInit];
	});
	
	return manager;
}

- (void)singletonInit {
	
	if ([self load:nil] == NO)
		_mutableCities = [NSMutableArray array];
}


#pragma mark - Adding/Removing

- (void)addCityWithID:(NSString *)cityID andName:(NSString *)cityName andCoordinates:(CLLocationCoordinate2D)coords {
	
	// Add the city
	CityData *newCity = [[CityData alloc] initWithCoordinates:coords andName:cityName andID:cityID];
	
	[self willChangeValueForKey:@"cities"];
	[_mutableCities addObject:newCity];
	[self didChangeValueForKey:@"cities"];
	
	[self save:nil];
}

- (void)removeCity:(CityData *)city{
	
	[self willChangeValueForKey:@"cities"];
	[_mutableCities removeObject:city];
	[self didChangeValueForKey:@"cities"];
	
	[self save:nil];
}


#pragma mark - Properties

- (NSArray *)cities {
	
	return [NSArray arrayWithArray:_mutableCities];
}


#pragma mark - Helpers

- (NSString *)filepath {
	
	NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
	
	return [[documentsURL URLByAppendingPathComponent:@"Cities.plist"] path];
}

- (BOOL)save:(NSError *)error {
	
	NSData *citiesData = [NSKeyedArchiver archivedDataWithRootObject:_mutableCities];
	if (!citiesData)
		return NO;
	
	BOOL success = [citiesData writeToFile:self.filepath options:NSDataWritingAtomic error:&error];
	
	return success;
}

- (BOOL)load:(NSError *)error {
	
	NSData *citiesData = [NSData dataWithContentsOfFile:self.filepath options:0 error:&error];
	if (!citiesData)
		return NO;
	
	_mutableCities = [NSKeyedUnarchiver unarchiveObjectWithData:citiesData];
	if (!_mutableCities)
		return NO;
	
	return YES;
}


@end









