//
//  WeatherClient.h
//  OpenWeatherApp
//
//  Created by Δημήτρης on 21/4/17.
//  Copyright © 2017 Dimitris Roilidis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WeatherData.h"

@import CoreLocation;

@interface WeatherClient : NSObject

+ (instancetype)sharedClient;

- (void)searchNearestCityForCoordinates:(CLLocationCoordinate2D)coords
						   withCallback:( void (^)( NSError* error, NSString *cityID, NSString *cityName ) )callback;

- (void)weatherForCoordinates:(CLLocationCoordinate2D)coords
				 withCallback:( void (^)( NSError* error, WeatherData *data ) )callback;
- (void)weatherForCityID:(NSString *)cityID
			withCallback:( void (^)( NSError* error, WeatherData *data ) )callback;

- (void)forecastForCityID:(NSString *)cityID
			 withCallback:(void (^)( NSError* error, NSArray<WeatherData *> *dataArray )) callback;


@end
