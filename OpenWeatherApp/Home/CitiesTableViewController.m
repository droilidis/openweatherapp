//
//  CitiesTableViewController.m
//  OpenWeatherApp
//
//  Created by Δημήτρης on 21/4/17.
//  Copyright © 2017 Dimitris Roilidis. All rights reserved.
//

#import "CitiesTableViewController.h"
#import "CitiesTableViewCell.h"
#import "WeatherClient.h"
#import "CityManager.h"
#import "DetailViewController.h"

@interface CitiesTableViewController () <CLLocationManagerDelegate>

@property (strong) NSArray<CityData *> *cities;
@property (strong) CLLocationManager *locationManager;
@property (strong) CLLocation *currentLocation;
@property (strong) CityData *currentCity;

@end

@implementation CitiesTableViewController

- (void)viewDidLoad {
	
    [super viewDidLoad];
	
	// Load cities
	self.cities = [[CityManager sharedManager] cities];
	if (!self.cities)
		self.cities = @[];
	
	// Observe model changes
	[[CityManager sharedManager] addObserver:self forKeyPath:@"cities" options:0 context:nil];
	
	// Register nib as table view cells
	[self.tableView registerNib:[UINib nibWithNibName:@"CitiesTableViewCell" bundle:nil] forCellReuseIdentifier:@"cities.cell"];
	
	// Set location manager
	self.locationManager = [CLLocationManager new];
	
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	
	// Get the new view controller using [segue destinationViewController].
	// Pass the selected object to the new view controller.
	if ([segue.identifier isEqualToString:@"cities.push.details"]) {
		
		// The sender object for this segue is the city object
		DetailViewController *detailVC = (DetailViewController *)segue.destinationViewController;
		detailVC.city = (CityData *)sender;
	}
}


#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
	
	if (object == [CityManager sharedManager] && [keyPath isEqualToString:@"cities"]) {
		
		// Locally save cities
		self.cities = nil;
		self.cities = [[CityManager sharedManager] cities];
		
		// And refresh UI
		// Reload the favorites section
		if (self.tableView.numberOfSections == 1 || self.cities.count == 0)
			[self.tableView reloadData];
		else
			[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:NO];
	}
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	
	return 1 + (self.cities.count ? 1 : 0);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

	return (section == 0 ? 1 : self.cities.count);
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
    CitiesTableViewCell *cell = (CitiesTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cities.cell" forIndexPath:indexPath];
    
    // Configure the cell...
	if (indexPath.section == 0) {
		
		// Current location cell
		// Fire update for the current location
		if (self.currentLocation) {
			
			// Get the nearest city
			[[WeatherClient sharedClient] searchNearestCityForCoordinates:self.currentLocation.coordinate withCallback:^(NSError *error, NSString *cityID, NSString *cityName) {
				
				if (error) {
					
					cell.cityNameLabel.text = @"No city nearby";
					return;
				}
				
				// Get the city's weather
				self.currentCity = [[CityData alloc] initWithCoordinates:self.currentLocation.coordinate andName:cityName andID:cityID];
				cell.city = self.currentCity;
			}];
			
			self.locationManager.delegate = nil;
		}
		
		else {
			// We need to get current location first
			cell.cityNameLabel.text = @"Fetching current location";
			
			self.locationManager.delegate = self;
			[self.locationManager requestLocation];
		}

	}
	
	else {
		
		// Cities cells
		cell.city = [self.cities objectAtIndex:indexPath.row];
	}
    
    return cell;
}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//	
//	if (section == 0)
//		return @"Current location";
//	
//	else return @"Favorites";
//}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
	
    // Return NO if you do not want the specified item to be editable.
	if (indexPath.section == 0)
		return NO;
	
    return YES;
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	
    if (editingStyle == UITableViewCellEditingStyleDelete) {
		
        // Delete the object from the data source
		[[CityManager sharedManager] removeCity:[self.cities objectAtIndex:indexPath.row]];
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
	}
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	CityData *city;
	if (indexPath.section == 0)
		city = self.currentCity;
	else
		city = [self.cities objectAtIndex:indexPath.row];
	
	self.selectionCallback(city);
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
	
	CityData *city;
	if (indexPath.section == 0) {
		// Current location
		city = self.currentCity;
	}
	
	else {
		city = [self.cities objectAtIndex:indexPath.row];
	}

	[self performSegueWithIdentifier:@"cities.push.details" sender:city];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	
	return 40.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	
	// Header is one image and one label
	UIView *header = [[UIView alloc] initWithFrame:CGRectZero];
	
	UIImage *image;
	NSString *title;
	
	if (section == 0) {
		
		image = [UIImage imageNamed:@"pin"];
		title = @"Current location";
	}
	
	else {
		
		image = [UIImage imageNamed:@"star"];
		title = @"Favorites";
	}

	UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
	imageView.contentMode = UIViewContentModeScaleAspectFit;
	
	UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	titleLabel.text = title;
	titleLabel.font = [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
	
	imageView.translatesAutoresizingMaskIntoConstraints = NO;
	titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
	
	[header addSubview:imageView];
	[header addSubview:titleLabel];
	
	[header addConstraint:[NSLayoutConstraint constraintWithItem:imageView
													   attribute:NSLayoutAttributeTop
													   relatedBy:NSLayoutRelationEqual
														  toItem:header
													   attribute:NSLayoutAttributeTopMargin
													  multiplier:1
														constant:0.0]];
	[header addConstraint:[NSLayoutConstraint constraintWithItem:imageView
													   attribute:NSLayoutAttributeBottom
													   relatedBy:NSLayoutRelationEqual
														  toItem:header
													   attribute:NSLayoutAttributeBottomMargin
													  multiplier:1
														constant:0.0]];
	[header addConstraint:[NSLayoutConstraint constraintWithItem:imageView
													   attribute:NSLayoutAttributeLeading
													   relatedBy:NSLayoutRelationEqual
														  toItem:header
													   attribute:NSLayoutAttributeLeadingMargin
													  multiplier:1
														constant:5.0]];
	[header addConstraint:[NSLayoutConstraint constraintWithItem:imageView
													   attribute:NSLayoutAttributeTrailing
													   relatedBy:NSLayoutRelationEqual
														  toItem:titleLabel
													   attribute:NSLayoutAttributeLeading
													  multiplier:1
														constant:-8.0]];
	[header addConstraint:[NSLayoutConstraint constraintWithItem:imageView
													   attribute:NSLayoutAttributeCenterY
													   relatedBy:NSLayoutRelationEqual
														  toItem:titleLabel
													   attribute:NSLayoutAttributeCenterY
													  multiplier:1
														constant:0.0]];
	
	header.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.75];
	
	return header;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
	
	self.currentLocation = locations.lastObject;
	
	// Reload the current location row
	[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
	
	NSLog(@"Location error: %@", error);
}


@end





